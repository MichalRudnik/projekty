require 'test_helper'

class Admin::RoadNamesControllerTest < ActionController::TestCase
  setup do
    @admin_road_name = admin_road_names(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_road_names)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_road_name" do
    assert_difference('Admin::RoadName.count') do
      post :create, admin_road_name: {  }
    end

    assert_redirected_to admin_road_name_path(assigns(:admin_road_name))
  end

  test "should show admin_road_name" do
    get :show, id: @admin_road_name
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_road_name
    assert_response :success
  end

  test "should update admin_road_name" do
    patch :update, id: @admin_road_name, admin_road_name: {  }
    assert_redirected_to admin_road_name_path(assigns(:admin_road_name))
  end

  test "should destroy admin_road_name" do
    assert_difference('Admin::RoadName.count', -1) do
      delete :destroy, id: @admin_road_name
    end

    assert_redirected_to admin_road_names_path
  end
end
