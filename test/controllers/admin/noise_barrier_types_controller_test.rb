require 'test_helper'

class Admin::NoiseBarrierTypesControllerTest < ActionController::TestCase
  setup do
    @admin_noise_barrier_type = admin_noise_barrier_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_noise_barrier_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_noise_barrier_type" do
    assert_difference('Admin::NoiseBarrierType.count') do
      post :create, admin_noise_barrier_type: {  }
    end

    assert_redirected_to admin_noise_barrier_type_path(assigns(:admin_noise_barrier_type))
  end

  test "should show admin_noise_barrier_type" do
    get :show, id: @admin_noise_barrier_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_noise_barrier_type
    assert_response :success
  end

  test "should update admin_noise_barrier_type" do
    patch :update, id: @admin_noise_barrier_type, admin_noise_barrier_type: {  }
    assert_redirected_to admin_noise_barrier_type_path(assigns(:admin_noise_barrier_type))
  end

  test "should destroy admin_noise_barrier_type" do
    assert_difference('Admin::NoiseBarrierType.count', -1) do
      delete :destroy, id: @admin_noise_barrier_type
    end

    assert_redirected_to admin_noise_barrier_types_path
  end
end
