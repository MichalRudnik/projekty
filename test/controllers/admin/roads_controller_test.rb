require 'test_helper'

class Admin::RoadsControllerTest < ActionController::TestCase
  setup do
    @admin_road = admin_roads(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_roads)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_road" do
    assert_difference('Admin::Road.count') do
      post :create, admin_road: {  }
    end

    assert_redirected_to admin_road_path(assigns(:admin_road))
  end

  test "should show admin_road" do
    get :show, id: @admin_road
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_road
    assert_response :success
  end

  test "should update admin_road" do
    patch :update, id: @admin_road, admin_road: {  }
    assert_redirected_to admin_road_path(assigns(:admin_road))
  end

  test "should destroy admin_road" do
    assert_difference('Admin::Road.count', -1) do
      delete :destroy, id: @admin_road
    end

    assert_redirected_to admin_roads_path
  end
end
