Bid::Application.routes.draw do
  root "projects#index"
  get "projects/index"
  
  devise_for :users
  
  resources :projects do
    resources :comments do
        delete "delete"
    end
    member do
        get "show_offers"
        post "comment"
    end
  end

  namespace :admin do
    resources :roads
    resources :projects
    resources :users
    resources :companies
    resources :noise_barrier_types
    resources :cities
    resources :road_names
    resources :offers
  end

resources :companies
resources :offers
resources :comments

resources :admin
resources :searches

end
