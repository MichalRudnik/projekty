class Offer < ActiveRecord::Base
  validates :offer_no, :offer_date, :project_id, :company_id, presence: true
  # 
  scope :by_ids, lambda {|ids|
    where(:id => ids) unless ids.empty?
  }
  # 
  belongs_to :project
  belongs_to :company
  # 
  self.per_page = 20
  # 
  default_scope order("offer_no ASC")
  
end
