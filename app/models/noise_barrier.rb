class NoiseBarrier < ActiveRecord::Base
  validates :quantity, :noise_barrier_type_id, presence: true

  belongs_to :road
  belongs_to :noise_barrier_type

  has_many :noise_barrier_contractors, dependent: :destroy  
  has_many :companies, through: :noise_barrier_contractors
  
  has_one :project, through: :road
  has_one :road_name, through: :road
  # 
  accepts_nested_attributes_for :noise_barrier_contractors

end
