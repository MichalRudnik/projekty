class NoiseBarrierType < ActiveRecord::Base
  validates :name, presence: true

  has_many :noise_barriers, dependent: :destroy
  # 
  default_scope order("name ASC")
  self.per_page = 20
  
end
