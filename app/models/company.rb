class Company < ActiveRecord::Base
  validates :name, :city_id, presence: true

  scope :by_ids, lambda {|ids|
    where(:id => ids) unless ids.empty?
  }

  belongs_to :city
  has_many :projects
  has_many :offers, dependent: :destroy
  has_many :noise_barrier_contractors, dependent: :destroy
  has_many :noise_barriers, through: :noise_barrier_contractors
  # 
  self.per_page = 20
  # 
  default_scope order("name ASC")
end
