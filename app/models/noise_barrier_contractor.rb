class NoiseBarrierContractor < ActiveRecord::Base
  # validates :company_id, :project_id, :noise_barrier_id, presence: true

  belongs_to :company
  belongs_to :project
  belongs_to :noise_barrier
end
