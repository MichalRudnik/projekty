class RoadName < ActiveRecord::Base
	validates :name, presence: true

	has_many :roads, dependent: :destroy
	has_many :projects, through: :roads
  	has_many :noise_barriers, through: :roads
	# 
	self.per_page = 20
	# 
  	default_scope order("name ASC")

end
