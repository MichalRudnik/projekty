class City < ActiveRecord::Base
	validates :name, presence: true
	has_many :companies
	# 
  default_scope order("name ASC")
  # 
  self.per_page = 20
	
end
