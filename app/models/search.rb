class Search < ActiveRecord::Base

	self.per_page = 20

	def projects
		@projects ||= find_projects
	end

	def companies
		@companies ||= find_companies
	end

	def offers
		@offer ||= find_offers
	end

	def comments
		@comments ||= find_comments
	end

private

	def find_projects
		projects = Project.order(:name)
		projects = projects.where("name like ?", "%#{title}%") if title.present?
		projects = projects.where(company_id: gw_id) if gw_id.present?
		projects = projects.gw(gw_id) if gw_id.present?
  		projects = projects.includes(:roads).where('roads.road_name_id' => road_id) if road_id.present?
  		projects = projects.includes(roads: :noise_barriers).where('noise_barriers.noise_barrier_type_id' => noise_barrier_type_id) if noise_barrier_type_id.present?
  		projects = projects.includes(:noise_barrier_contractors).where('noise_barrier_contractors.company_id' => noise_barrier_contractor_id).uniq if noise_barrier_contractor_id.present?
		projects
	end

	def find_companies
		companies = Company.order(:name)
		companies = companies.where("name like ?", "%#{title}%") if title.present?
		companies = companies.where(city_id: road_id) if road_id.present?
		companies
	end	
	
	def find_offers
		offers = Offer.order(:offer_no)
		offers = offers.includes(:project).where("projects.name like ?", "%#{title}%") if title.present?
		offers = offers.where(company_id: gw_id) if gw_id.present?
		offers
	end	

	def find_comments
		comments = Comment.order(:created_at)
		comments = comments.where("comment like ?", "%#{title}%") if title.present?
		comments
	end	
end
