class Comment < ActiveRecord::Base
  validates :project_id, presence: true
  validates :comment, presence: true
  belongs_to :project
	# 
  scope :by_ids, lambda {|ids|
    where(:id => ids) unless ids.empty?
  }
  # 
  default_scope order("created_at DESC")

end
