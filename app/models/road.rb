class Road < ActiveRecord::Base
  validates :road_name_id, presence: true
  delegate :name, to: :road_name
  belongs_to :project
  belongs_to :road_name
  has_many :noise_barriers, dependent: :destroy
  # 
  accepts_nested_attributes_for :noise_barriers, allow_destroy: true

end
