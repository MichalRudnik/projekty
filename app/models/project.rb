class Project < ActiveRecord::Base
  validates :name, presence: true
  validates :company_id, presence: true
  # 
  scope :by_ids, lambda {|ids|
    where(:id => ids) unless ids.empty?
  }
  # 
  belongs_to :company
  has_many :offers, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :roads, dependent: :destroy
  has_many :road_names, through: :roads
  has_many :noise_barrier_contractors, dependent: :destroy
  has_many :noise_barriers, through: :roads  
  # 
  self.per_page = 20
  # 
  scope :current, -> { where current: true }
  scope :not_current, -> { where current: false }
  scope :gw, -> (company_id) { where(company_id: company_id)}
  # 
  accepts_nested_attributes_for :roads, allow_destroy: true
  # 
end
