module ApplicationHelper

	def sortable(column, title = nil)
	  title ||= column.titleize
	  css_class = column.to_s == sort_column.to_s ? "current #{sort_direction}" : nil	  
	  direction = sort_direction == "asc" ? "desc" : "asc"
    
    Rails.logger.debug(sort_direction)
	  link_to title, params.merge({:sort => column, :direction => direction}), :class => css_class
	end

  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end
 
  # RailsCasts 197
  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end

  def admin_namespace?
    params[:controller].split("/").first == 'admin'
  end


end
