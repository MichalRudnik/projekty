module ProjectsHelper

	def search_helper search
		result = ""
		search.attributes.each { |key, value|
			if ((key == "title") && (!value.to_s.eql? ""))
				puts key.to_s + " >>>>>>>> " + value.to_s
				result = result.to_s + "Nazwa projektu: ".to_s + value.to_s + " ".to_s end
			if key == "gw_id" && !value.nil?
				result = result.to_s + "Generalny Wykonawca: ".to_s + Company.find(value).name.to_s + " ".to_s end
			if key == "road_id" && !value.nil?
				result = result.to_s + "Droga: ".to_s + RoadName.find(value).name.to_s + " ".to_s end
			if key == "noise_barrier_type_id" && !value.nil? 
				result = result.to_s + "Typ ekranów: " + NoiseBarrierType.find(value).name.to_s + " ".to_s end
			if key == "noise_barrier_contractor_id" && !value.nil? 
				result = result.to_s + "Wykonawca ekranów: ".to_s + Company.find(value).name.to_s + " ".to_s end
		}
		unless result == "" 
			result = "Wyszukiwanie >> ".to_s + result.to_s
		end
		result
	end
end
