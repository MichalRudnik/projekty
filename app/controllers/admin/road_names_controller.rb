class Admin::RoadNamesController < ApplicationController
  before_action :set_admin_road_name, only: [:show, :edit, :update, :destroy]
  before_filter do 
    redirect_to new_user_session_path unless current_user && current_user.is_admin
  end  
  # GET /admin/road_names
  # GET /admin/road_names.json
  def index
    @admin_road_names = RoadName.paginate(:page => params[:page])
  end

  # GET /admin/road_names/1
  # GET /admin/road_names/1.json

  # GET /admin/road_names/new
  def new
    @admin_road_name = RoadName.new
  end

  # GET /admin/road_names/1/edit
  def edit
  end

  # POST /admin/road_names
  # POST /admin/road_names.json
  def create
    @admin_road_name = RoadName.new(admin_road_name_params)

    respond_to do |format|
      if @admin_road_name.save
        format.html { redirect_to admin_road_names_path, notice: 'Droga lub lokalizacja została utworzona.' }
        format.json { render action: 'show', status: :created, location: @admin_road_name }
      else
        format.html { render action: 'new' }
        format.json { render json: @admin_road_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/road_names/1
  # PATCH/PUT /admin/road_names/1.json
  def update
    respond_to do |format|
      if @admin_road_name.update(admin_road_name_params)
        format.html { redirect_to admin_road_names_path, notice: 'Dane drogi lub lokalizacji zostały zaktualizowane.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @admin_road_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/road_names/1
  # DELETE /admin/road_names/1.json
  def destroy
    @admin_road_name.destroy
    respond_to do |format|
      format.html { redirect_to admin_road_names_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_road_name
      @admin_road_name = RoadName.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_road_name_params
      params.require(:road_name).permit!
    end
end
