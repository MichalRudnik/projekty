class Admin::CitiesController < ApplicationController
  before_action :set_admin_city, only: [:show, :edit, :update, :destroy]
  before_filter do 
    redirect_to new_user_session_path unless current_user && current_user.is_admin
  end    
  # GET /admin/cities
  # GET /admin/cities.json
  def index
    @admin_cities = City.paginate(:page => params[:page])
  end

  # GET /admin/cities/new
  def new
    @admin_city = City.new
  end

  # GET /admin/cities/1/edit
  def edit
  end

  # POST /admin/cities
  # POST /admin/cities.json
  def create
    @admin_city = City.new(admin_city_params)

    respond_to do |format|
      if @admin_city.save
        format.html { redirect_to admin_cities_path, notice: 'Miasto zostało dodane.' }
        format.json { render action: 'show', status: :created, location: @admin_city }
      else
        format.html { render action: 'new' }
        format.json { render json: @admin_city.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/cities/1
  # PATCH/PUT /admin/cities/1.json
  def update
    respond_to do |format|
      if @admin_city.update(admin_city_params)
        format.html { redirect_to admin_cities_path, notice: 'Miasto zostało zaktualizowane.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @admin_city.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/cities/1
  # DELETE /admin/cities/1.json
  def destroy
    @admin_city.destroy
    respond_to do |format|
      format.html { redirect_to admin_cities_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_city
      @admin_city = City.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_city_params
      params.require(:city).permit!
    end
end
