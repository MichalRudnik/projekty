class Admin::RoadsController < ApplicationController
  before_action :set_admin_road, only: [:show, :edit, :update, :destroy]
  before_filter do 
    redirect_to new_user_session_path unless current_user && current_user.is_admin
  end  

  # GET /admin/roads
  # GET /admin/roads.json
  def index
    @admin_roads = Road.all
  end
  # GET /admin/roads/new
  def new
    @admin_road = Road.new
  end

  # GET /admin/roads/1/edit
  def edit
  end

  # POST /admin/roads
  # POST /admin/roads.json
  def create
    @admin_road = Road.new(admin_road_params)

    respond_to do |format|
      if @admin_road.save
        format.html { redirect_to admin_roads_path, notice: 'Road was successfully created.' }
        format.json { render action: 'show', status: :created, location: @admin_road }
      else
        format.html { render action: 'new' }
        format.json { render json: @admin_road.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/roads/1
  # PATCH/PUT /admin/roads/1.json
  def update
    respond_to do |format|
      if @admin_road.update(admin_road_params)
        format.html { redirect_to admin_roads_path, notice: 'Road was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @admin_road.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/roads/1
  # DELETE /admin/roads/1.json
  def destroy
    @admin_road.destroy
    respond_to do |format|
      format.html { redirect_to admin_roads_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_road
      @admin_road = Road.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_road_params
      params.require(:road).permit!
    end
end
