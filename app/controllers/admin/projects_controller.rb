class Admin::ProjectsController < ApplicationController
  before_action :set_admin_project, only: [:show, :edit, :update, :destroy]
  before_filter do 
    redirect_to new_user_session_path unless current_user && current_user.is_admin
  end  

  # GET /admin/projects
  # GET /admin/projects.json
  def index
    @admin_projects = Project.paginate(:page => params[:page])
  end

  # GET /admin/projects/1
  # GET /admin/projects/1.json
  def show
    @project = @admin_project
    @offers = @project.offers
    @roads = @project.roads  
    @noisebarriers = @project.noise_barriers
    @comments = @project.comments
    @comment = Comment.new
    # @search = @project


  end

  # GET /admin/projects/new
  def new
    @admin_project = Project.new
    a = @admin_project.roads.build
    b = a.noise_barriers.build
    b.noise_barrier_contractors.build

  end

  # GET /admin/projects/1/edit
  def edit
  end

  # POST /admin/projects
  # POST /admin/projects.json
  def create
    @admin_project = Project.new(admin_project_params)
    puts "DEBUG " + admin_project_params.to_s

    respond_to do |format|
      if @admin_project.save
        # 
        # @admin_project.roads.each do |road|
        #  road.noise_barriers.each do |nb|
        #     nb.noise_barrier_contractors.each {|nbc| nbc.update_attribute(:project_id, @admin_project.id) }
        #   end
        # end


        # last.noise_barriers.last.noise_barrier_contractors.first.company.name
        # 
        format.html { redirect_to admin_project_path(@admin_project), notice: 'Projekt został dodany.' }
        format.json { render action: 'show', status: :created, location: @admin_project }
      else
        format.html { render action: 'new' }
        format.json { render json: @admin_project.errors, status: :unprocessable_entity }
      end
    end




  end

  # PATCH/PUT /admin/projects/1
  # PATCH/PUT /admin/projects/1.json
  def update
    respond_to do |format|
      if @admin_project.update(admin_project_params)
        format.html { redirect_to admin_projects_path, notice: 'Projekt został zaktualizowany.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @admin_project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/projects/1
  # DELETE /admin/projects/1.json
  def destroy
    @admin_project.destroy
    respond_to do |format|
      format.html { redirect_to admin_projects_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_project
      @admin_project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_project_params
      # params[:admin_project]
      params.require(:project).permit!

    end
end
