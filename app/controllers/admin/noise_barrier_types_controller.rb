class Admin::NoiseBarrierTypesController < ApplicationController
  before_action :set_admin_noise_barrier_type, only: [:show, :edit, :update, :destroy]
  before_filter do 
    redirect_to new_user_session_path unless current_user && current_user.is_admin
  end  
  
  # GET /admin/noise_barrier_types
  # GET /admin/noise_barrier_types.json
  def index
    @admin_noise_barrier_types = NoiseBarrierType.paginate(:page => params[:page])
  end

  # GET /admin/noise_barrier_types/new
  def new
    @admin_noise_barrier_type = NoiseBarrierType.new
  end

  # GET /admin/noise_barrier_types/1/edit
  def edit
  end

  # POST /admin/noise_barrier_types
  # POST /admin/noise_barrier_types.json
  def create
    @admin_noise_barrier_type = NoiseBarrierType.new(admin_noise_barrier_type_params)

    respond_to do |format|
      if @admin_noise_barrier_type.save
        format.html { redirect_to admin_noise_barrier_types_path, notice: 'Rodzaj ekranów został dodany.' }
      else
        format.html { render action: 'new' }
        format.json { render json: @admin_noise_barrier_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/noise_barrier_types/1
  # PATCH/PUT /admin/noise_barrier_types/1.json
  def update
    respond_to do |format|
      if @admin_noise_barrier_type.update(admin_noise_barrier_type_params)
        format.html { redirect_to admin_noise_barrier_types_path, notice: 'Rodzaj ekranów został zaktualizowany.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @admin_noise_barrier_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/noise_barrier_types/1
  # DELETE /admin/noise_barrier_types/1.json
  def destroy
    @admin_noise_barrier_type.destroy
    respond_to do |format|
      format.html { redirect_to admin_noise_barrier_types_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_noise_barrier_type
      @admin_noise_barrier_type = NoiseBarrierType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_noise_barrier_type_params
      params.require(:noise_barrier_type).permit!
    end
end
