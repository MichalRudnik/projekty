class Admin::OffersController < ApplicationController
  before_action :set_admin_offer, only: [:show, :edit, :update, :destroy]

  # GET /admin/offers
  # GET /admin/offers.json
  def index
    @admin_offers = Offer.paginate(:page => params[:page])
  end

  # GET /admin/offers/1
  # GET /admin/offers/1.json
  def show
  end

  # GET /admin/offers/new
  def new
    @admin_offer = Offer.new
  end

  # GET /admin/offers/1/edit
  def edit
  end

  # POST /admin/offers
  # POST /admin/offers.json
  def create
    @admin_offer = Offer.new(admin_offer_params)

    respond_to do |format|
      if @admin_offer.save
        format.html { redirect_to @admin_offer, notice: 'Oferta została dodana.' }
        format.json { render action: 'show', status: :created, location: @admin_offer }
      else
        format.html { render action: 'new' }
        format.json { render json: @admin_offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/offers/1
  # PATCH/PUT /admin/offers/1.json
  def update
    respond_to do |format|
      if @admin_offer.update(admin_offer_params)
        format.html { redirect_to @admin_offer, notice: 'Oferta została zaktualizowana.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @admin_offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/offers/1
  # DELETE /admin/offers/1.json
  def destroy
    @admin_offer.destroy
    respond_to do |format|
      format.html { redirect_to admin_offers_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_offer
      @admin_offer = Offer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_offer_params
      params[:admin_offer]
    end
end
