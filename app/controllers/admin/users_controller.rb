class Admin::UsersController < ApplicationController
  before_filter do 
    redirect_to new_user_session_path unless current_user && current_user.is_admin
  end  
  def index
    @users = User.paginate(:page => params[:page])
  end

  def edit
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end


  def update
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.update(admin_user_params)
        format.html { redirect_to admin_users_path, notice: 'Dane użytkownik został zaktualizowane.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @user = User.new(admin_user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_users_path, notice: 'Użytkownik został dodany.' }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  # DELETE /admin/roads/1
  # DELETE /admin/roads/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_users_url }
      format.json { head :no_content }
    end
  end


  private

  def admin_user_params
    params.require(:user).permit!
  end

end