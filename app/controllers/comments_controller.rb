class CommentsController < ApplicationController
	helper_method :sort_column, :sort_direction
  before_action :sort_column, :sort_direction, only: :index
	before_action :set_comment, only: [:delete]

	def index		
	    if params[:search]
				@search = Search.find(params[:search])
	      if @search.comments.empty?
	        @comments = nil
				else
        	@comments = Comment.unscoped.by_ids(@search.comments.map(&:id)).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)
		    end
      else
		    @comments ||= Comment.unscoped.paginate(:page => params[:page]).order(sort_column + " " + sort_direction)
		    @search = Search.new(page: "comment")     
	    end
	end

	def delete
		@comment.destroy
		redirect_to project_path(params[:project_id])
	end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def set_comment
    	@comment = Comment.find(params[:comment_id])
    end

    def sort_column
      Comment.column_names.include?(params[:sort]) ? params[:sort] : "created_at"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end

end