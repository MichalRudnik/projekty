class CompaniesController < ApplicationController
	helper_method :sort_column, :sort_direction
  before_action :sort_column, :sort_direction, only: :index

  def index    
    if params[:search]
	    @search = Search.find(params[:search])
      if !@search.companies.empty?
    	  @companies = Company.unscoped.by_ids(@search.companies.map(&:id)).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)
      end
    else 
	  	@companies ||= Company.unscoped.paginate(:page => params[:page]).order(sort_column + " " + sort_direction)
    	@search = Search.new(page: "company")      
    end
  end

	private

	  def sort_column
	    Company.column_names.include?(params[:sort]) ? params[:sort] : "name"
	  end
	  
	  def sort_direction
	    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	  end

end