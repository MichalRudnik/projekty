class SearchesController < ApplicationController
  before_action :set_search, only: [:create, :update]

  def new
    @search = Search.new
  end

  def create

  end

  def update

  end

  private

  def set_search
    @search = Search.create!(search_params)
    if @search.page == "project"
      @projects = @search.projects.paginate(:page => params[:page])
      redirect_to projects_path(:search => @search.id)
    end
    if @search.page == "company"
      @companies = @search.companies.paginate(:page => params[:page])
      redirect_to companies_path(:search => @search.id)
    end
    if @search.page == "offer"
      @offers = @search.offers.paginate(:page => params[:page])
      redirect_to offers_path(:search => @search.id)
    end
    if @search.page == "comment"
      @comments = @search.comments.paginate(:page => params[:page])
      redirect_to comments_path(:search => @search.id)
    end
  end

  def search_params
    params.require(:search).permit!
  end

end
