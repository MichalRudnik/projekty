class OffersController < ApplicationController
	helper_method :sort_column, :sort_direction
	before_action :authenticate_user!, only: [:index]
     
	def index		
	    if params[:search]
				@search = Search.find(params[:search])
		      if @search.offers.empty?
		        @offers = nil
					else
		        @offers =  Offer.unscoped.by_ids(@search.offers.map(&:id)).paginate(:page => params[:page]).order(sort_column + " " + sort_direction)
			    end
      else
		    @offers ||= Offer.unscoped.paginate(:page => params[:page]).order(sort_column + " " + sort_direction)
		    @search = Search.new(page: "offer")     
	    end
	end

	private

	  def sort_column
	    Offer.column_names.include?(params[:sort]) ? params[:sort] : "offer_no"
	  end
	  
	  def sort_direction
	    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
	  end

end