class ProjectsController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :sort_column, :sort_direction, only: :index
	before_action :set_project, only: [:show, :show_offers, :comment]

  def comment 
    if comment_params[:comment].length > 0
      @comment = @project.comments.new(comment_params) 
      if @comment.save
        redirect_to @project
      else
        render :show
      end
    else
        redirect_to @project
    end
  end

  def destroy
  end
  
  def index    
    if params[:search]
      @search = Search.find(params[:search])
      if @search.projects.empty?
        @projects = nil
      else
        @projects = Project.by_ids(@search.projects.map(&:id)).page(params[:page]).order(sort_column + " " + sort_direction)
      end
    else 
      @projects ||= Project.paginate(:page => params[:page]).order(sort_column + " " + sort_direction)  
      @search = Search.new(page: "project")      
    end
  end

  def show
    @comment = Comment.new
    @search = @project
  end

  def show_offers
  	@offers = @project.offers
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
      @offers = @project.offers
      @roads = @project.roads 
      @noisebarriers = @project.noise_barriers
      @comments = @project.comments
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:id, :name, :noisebarrier_id, :company_id)
    end

    def sort_column
      Project.column_names.include?(params[:sort]) ? params[:sort] : "name"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
    end

    def comment_params
      params.require(:comment).permit!
    end
end
