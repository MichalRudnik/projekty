class AdminController < ApplicationController
  before_filter do 
    redirect_to new_user_session_path unless current_user && current_user.is_admin
  end    

	def index		

	end

end