json.array!(@admin_roads) do |admin_road|
  json.extract! admin_road, :id
  json.url admin_road_url(admin_road, format: :json)
end
