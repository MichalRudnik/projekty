json.array!(@admin_offers) do |admin_offer|
  json.extract! admin_offer, :id
  json.url admin_offer_url(admin_offer, format: :json)
end
