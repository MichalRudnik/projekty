json.array!(@admin_road_names) do |admin_road_name|
  json.extract! admin_road_name, :id
  json.url admin_road_name_url(admin_road_name, format: :json)
end
