json.array!(@admin_noise_barrier_types) do |admin_noise_barrier_type|
  json.extract! admin_noise_barrier_type, :id
  json.url admin_noise_barrier_type_url(admin_noise_barrier_type, format: :json)
end
