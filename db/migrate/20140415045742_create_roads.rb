class CreateRoads < ActiveRecord::Migration
  def change
    create_table :roads do |t|
      t.references :project, index: true
      t.references :road_name, index: true

      t.timestamps
    end
  end
end
