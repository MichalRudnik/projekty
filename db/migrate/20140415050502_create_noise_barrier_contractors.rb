class CreateNoiseBarrierContractors < ActiveRecord::Migration
  def change
    create_table :noise_barrier_contractors do |t|
      t.references :company, index: true
      t.references :project, index: true
      t.references :noise_barrier, index: true

      t.timestamps
    end
  end
end
