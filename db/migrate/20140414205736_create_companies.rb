class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|      
      t.string :name
      t.string :phone
      t.string :email
      t.string :website
      t.string :street
      t.string :zip_code
      t.references :city, index: true

      t.timestamps
    end
  end
end
