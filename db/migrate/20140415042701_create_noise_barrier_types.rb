class CreateNoiseBarrierTypes < ActiveRecord::Migration
  def change
    create_table :noise_barrier_types do |t|
      t.string :name

      t.timestamps
    end
  end
end
