class CreateRoadNames < ActiveRecord::Migration
  def change
    create_table :road_names do |t|
      t.string :name

      t.timestamps
    end
  end
end
