class CreateNoiseBarriers < ActiveRecord::Migration
  def change
    create_table :noise_barriers do |t|
      t.integer :quantity
      t.references :road, index: true
      t.references :noise_barrier_type, index: true

      t.timestamps
    end
  end
end
