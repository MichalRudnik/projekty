class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.datetime :tender_opening_date
      t.string :lead_time
      t.boolean :current
      t.boolean :flag
      t.references :company, index: true

      t.timestamps
    end
  end
end
