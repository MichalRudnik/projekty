class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.string :title
      t.integer :gw_id
      t.boolean :current
      t.integer :road_id
      t.integer :noise_barrier_type_id
      t.integer :noise_barrier_contractor_id

      t.timestamps
    end
  end
end
