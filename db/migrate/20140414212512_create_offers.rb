class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.integer :offer_no
      t.date :offer_date
      t.references :project, index: true
      t.references :company, index: true

      t.timestamps
    end
  end
end
