# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140728192803) do

  create_table "cities", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", force: true do |t|
    t.text     "comment"
    t.integer  "project_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["project_id"], name: "index_comments_on_project_id"

  create_table "companies", force: true do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "website"
    t.string   "street"
    t.string   "zip_code"
    t.integer  "city_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "companies", ["city_id"], name: "index_companies_on_city_id"

  create_table "noise_barrier_contractors", force: true do |t|
    t.integer  "company_id"
    t.integer  "project_id"
    t.integer  "noise_barrier_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "noise_barrier_contractors", ["company_id"], name: "index_noise_barrier_contractors_on_company_id"
  add_index "noise_barrier_contractors", ["noise_barrier_id"], name: "index_noise_barrier_contractors_on_noise_barrier_id"
  add_index "noise_barrier_contractors", ["project_id"], name: "index_noise_barrier_contractors_on_project_id"

  create_table "noise_barrier_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "noise_barriers", force: true do |t|
    t.integer  "quantity"
    t.integer  "road_id"
    t.integer  "noise_barrier_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "noise_barriers", ["noise_barrier_type_id"], name: "index_noise_barriers_on_noise_barrier_type_id"
  add_index "noise_barriers", ["road_id"], name: "index_noise_barriers_on_road_id"

  create_table "offers", force: true do |t|
    t.integer  "offer_no"
    t.date     "offer_date"
    t.integer  "project_id"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "offers", ["company_id"], name: "index_offers_on_company_id"
  add_index "offers", ["project_id"], name: "index_offers_on_project_id"

  create_table "projects", force: true do |t|
    t.string   "name"
    t.datetime "tender_opening_date"
    t.string   "lead_time"
    t.boolean  "current"
    t.boolean  "flag"
    t.integer  "company_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "projects", ["company_id"], name: "index_projects_on_company_id"

  create_table "road_names", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roads", force: true do |t|
    t.integer  "project_id"
    t.integer  "road_name_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roads", ["project_id"], name: "index_roads_on_project_id"
  add_index "roads", ["road_name_id"], name: "index_roads_on_road_name_id"

  create_table "searches", force: true do |t|
    t.string   "title"
    t.integer  "gw_id"
    t.boolean  "current"
    t.integer  "road_id"
    t.integer  "noise_barrier_type_id"
    t.integer  "noise_barrier_contractor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "page"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_admin",               default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
